<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>OOP PHP</title>
</head>
<body>
	<h1>OOP PHP</h1>

	<?php
		require_once 'animal.php';
		require_once 'ape.php';
		require_once 'frog.php';
	
	echo 'Release 0 : <br>';

	$sheep = new Animal("shaun");
	echo $sheep->name . '<br>'; // "shaun"
	echo $sheep->legs . '<br>'; // 2
	echo $sheep->cold_blooded . '<br>'; // false

	echo '<br> Release 1 : <br>';
	
	$sungokong = new Ape("kera sakti");
	echo $sungokong->name . '<br>'; // "kera sakti"
	echo $sungokong->legs . '<br>'; //2
	echo $sungokong->yell() . '<br>'; // "Auooo"
	echo '<br>';
	$kodok = new Frog("buduk");
	echo $kodok->name . '<br>'; // "buduk"
	echo $kodok->legs . '<br>'; //4
	echo $kodok->jump() . '<br>'; // "hop hop"
	?>
</body>
</html>